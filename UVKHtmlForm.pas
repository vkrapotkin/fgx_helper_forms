unit UVKHtmlForm;

interface

{$SCOPEDENUMS ON}

uses
  System.Types, System.Classes, FGX.Forms, FGX.Forms.Types, FGX.Controls, FGX.Controls.Types,
  USelfDestroyForm, FGX.NavigationBar.Types, FGX.Layout.Types, FGX.WebBrowser,
  FGX.StaticLabel, FGX.Image, FGX.ActivityIndicator, FGX.Layout,
  FGX.NavigationBar, FGX.Platform, FGX.Timer, FGX.Application.Events;

type

  TVKHtmlForm = class(TSelfDestroyForm)
    wb1: TfgWebBrowser;
  public
    HTML:string;
    URL:string;
    procedure AfterLoad; override;
  end;

implementation

uses
  system.sysutils;

{$R *.xfm}

{ Tso2HtmlForm }

procedure TVKHtmlForm.AfterLoad;
begin
  if Html <> '' then
    wb1.LoadHTML(HTML,URL)
  else
    wb1.LoadRequest(URL);

  inherited AfterLoad;
end;


end.
