unit USelfDestroyForm;

interface

{$SCOPEDENUMS ON}

uses
  System.Types,
  System.Classes,
  system.uitypes,
  system.sysutils,

  FGX.Forms,
  FGX.Forms.Types,
  FGX.Controls,
  FGX.Controls.Types,
  FGX.Layout,
  FGX.Layout.Types,
  FGX.NavigationBar.Types,
  FGX.NavigationBar,
  FGX.Animation,
  FGX.Animation.Types,
  FGX.ActivityIndicator,
  FGX.StaticLabel,
  FGX.Image,
  FGX.Platform,
  FGX.Application.Events,
  FGX.Timer;

type
  TSelfDestroyForm = class(TfgForm)
    nav1: TfgNavigationBar;
    layAnimation: TfgLayout;
    aniAnimation: TfgActivityIndicator;
    layNoData: TfgLayout;
    imgNoData: TfgImage;
    lblNoData: TfgLabel;
    fgApplicationEvents1: TfgApplicationEvents;
    lblAniComment: TfgLabel;
    tmrAnimatorCount: TfgTimer;
    lblAnimationCounter: TfgLabel;
    layAnimationCenter: TfgLayout;
    procedure fgFormKey(Sender: TObject; const AKey: TfgKey; var AHandled: Boolean);
    procedure nav1NavigationIconTap(Sender: TObject);
    procedure fgApplicationEvents1LowMemory(Sender: TObject);
    procedure tmrAnimatorCountTimer(Sender: TObject);
  public
    const
      EXCEPTION_CODE = 1000;
  protected
    FLoading: Boolean;
    FDestroying: Boolean;
    procedure SetLoading(const Value: Boolean); virtual;
    procedure ThreadSaveException(E: Exception); virtual;
    procedure ThreadLoadException(E: Exception); virtual;
  public
    FReloadProc: TThreadMethod;
    CanCloseWhileException: Boolean;
    ShowAfterLoad: Boolean;
    procedure AnimationStart(comment: string = ''); virtual;
    procedure AnimationComment(comment: string); virtual;
    procedure AnimationStop; virtual;
    procedure BeforeSave; virtual;
    procedure ThreadSave; virtual;
    procedure AfterSave; virtual;
    procedure DoExit; virtual;
    procedure ThreadLoad; virtual;
    procedure AfterLoad; virtual;
    procedure Load; virtual;
    procedure CloseForm;
    procedure ShowForm(ATitle: string = '');
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property Loading: Boolean read FLoading write SetLoading;
  end;

implementation

{$R *.xfm}

uses
  FGX.Application,
  FGX.Dialogs,
  FGX.Log,

  System.threading,
  UMsgForm;

{ TSelfDestroyForm }

procedure TSelfDestroyForm.SetLoading(const Value: Boolean);
begin
  FLoading := Value;
end;

procedure TSelfDestroyForm.ShowForm(ATitle: string = '');
begin
  Load;
  if ATitle <> '' then
    nav1.Title := ATitle;
  show;
//  TfgAnimationHelper.ShowModalForm(self);
end;

procedure TSelfDestroyForm.ThreadSaveException(e: exception);
begin
end;

procedure TSelfDestroyForm.CloseForm;
begin
  BeforeSave;
  AnimationStart();
  ttask.Run(
    procedure
    begin
      try
        ThreadSave();
      except
        on e: Exception do
        begin
          ThreadSaveException(e);
        end;
      end;

      if CanCloseWhileException then
      begin
        TThread.Synchronize(nil,
          procedure
          begin
            AfterSave();
            DoExit();
            if Assigned(FReloadProc) then
              FReloadProc;
          end);
      end;
    end);
end;

procedure TSelfDestroyForm.DoExit;
begin
  AnimationStop;
//  __ObjRelease;
  if Parent <> NIL  then
    TThread.ForceQueue(nil, procedure begin free; end)
  else
    TfgAnimationHelper.HideModalForm(Self, [TfgAnimationOption.ReleaseControlOnFinish]);
  Parent := nil;
end;

constructor TSelfDestroyForm.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  CanCloseWhileException := true;
  layNoData.Visible := False;
  ShowAfterLoad := true;
//  __ObjAddRef;
end;

destructor TSelfDestroyForm.Destroy;
begin
//  __ObjRelease;
  FDestroying := true;
  inherited;
end;

procedure TSelfDestroyForm.fgApplicationEvents1LowMemory(Sender: TObject);
begin
//  TMsgForm.ShowError('������', '������� ��������, ��� �������� ���� ������.');
end;

procedure TSelfDestroyForm.fgFormKey(Sender: TObject; const AKey: TfgKey; var AHandled: Boolean);
begin
  if (parent = NIL) and (AKey.Action = TfgKeyAction.Up) and (AKey.Code = vkHardwareBack) then
  begin
    AHandled := True;
    if (not Loading) then
      CloseForm;
  end;
end;

procedure TSelfDestroyForm.AfterSave;
begin
  AnimationStop;
end;

procedure TSelfDestroyForm.AnimationComment(comment: string);
begin
  if FDestroying then
    exit;

  if TThread.CurrentThread.ThreadID <> MainThreadID then
  begin
    tthread.synchronize(nil,
      procedure
      begin
        lblAniComment.Text := comment;
        lblAniComment.Visible := comment <> '';
      end);
  end
  else
  begin
    lblAniComment.Text := comment;
    lblAniComment.Visible := comment <> '';
  end;
end;

procedure TSelfDestroyForm.AnimationStart(comment: string = '');
begin
  if FDestroying then
    exit;

  layAnimation.Visible := true;
  aniAnimation.IsAnimating := true;
  lblAnimationCounter.text := '';
  tmrAnimatorCount.Tag := 0;
  AnimationComment(comment);
  tmrAnimatorCount.Enabled := true;
end;

procedure TSelfDestroyForm.AnimationStop();
begin
  if FDestroying then
    exit;

  layAnimation.Visible := false;
  aniAnimation.IsAnimating := false;
  tmrAnimatorCount.Enabled := false;
end;

procedure TSelfDestroyForm.BeforeSave;
begin
  AnimationStart();
end;

procedure TSelfDestroyForm.AfterLoad;
begin
  AnimationStop();
  Loading := False;
  if ShowAfterLoad then
    show;
  AnimationComment('����� �������� ��������� ������');
end;

procedure TSelfDestroyForm.Load;
begin
  AnimationStart();
  ttask.Run(
    procedure
    begin
      try
        ThreadLoad;
      except
        on e: Exception do
        begin
          ThreadloadException(e);
        end;
      end;
      TThread.Queue(nil, AfterLoad);
    end);
end;

procedure TSelfDestroyForm.nav1NavigationIconTap(Sender: TObject);
begin
  if not Loading then
    CloseForm;
end;

procedure TSelfDestroyForm.ThreadSave;
begin
end;

procedure TSelfDestroyForm.tmrAnimatorCountTimer(Sender: TObject);
begin
  tmrAnimatorCount.tag := tmrAnimatorCount.tag + 1;
  lblAnimationCounter.Text := tmrAnimatorCount.tag.ToString;
end;

procedure TSelfDestroyForm.ThreadLoad;
begin
  TThread.Synchronize(nil,
    procedure
    begin
      Loading := true;
    end);
end;

procedure TSelfDestroyForm.ThreadLoadException(E: Exception);
begin
end;

end.

