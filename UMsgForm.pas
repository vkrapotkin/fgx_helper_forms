unit UMsgForm;

interface

{$SCOPEDENUMS ON}

uses
  System.Types, System.Classes, FGX.Forms, FGX.Forms.Types, FGX.Controls, FGX.Controls.Types, FGX.Layout,
  FGX.Layout.Types, FGX.NavigationBar.Types, FGX.StaticLabel, FGX.NavigationBar,
  USelfDestroyForm, FGX.ActivityIndicator, FGX.Image, FGX.Memo, FGX.Platform,
  FGX.Application.Events, FGX.Timer;

type
  TMsgForm = class(TSelfDestroyForm)
    mMessage: TfgMemo;
    procedure nav1ActionButtons0Tap(Sender: TObject);
  private
    FOnDoneTap: TNotifyEvent;
  public
    class procedure ShowError(Title, Msg:string);
    class procedure ShowMsg(Title, Msg:string);
    constructor Create(AOwner: TComponent); override;

    property OnDoneTap: TNotifyEvent read FOnDoneTap write FOnDoneTap;
  end;


implementation

{$R *.xfm}

uses
  System.SysUtils, FGX.Application, FGX.Dialogs, FGX.Log, fgx.animation, system.uitypes;


constructor TMsgForm.Create(AOwner: TComponent);
begin
  inherited;
  nav1.ActionButtons.ByName['Done'].Visible := False;
end;

procedure TMsgForm.nav1ActionButtons0Tap(Sender: TObject);
begin
  if Assigned(FOnDoneTap) then
    FOnDoneTap(Sender);
end;

class procedure TMsgForm.ShowError(Title, Msg:string);
var
  MsgForm: TMsgForm;
begin
  MsgForm :=  TMsgForm.Create(NIL);
  MsgForm.mMessage.Text := msg;
  MsgForm.nav1.BackgroundName := 'Theme\Error';
  MsgForm.ShowForm(title);
end;

class procedure TMsgForm.ShowMsg(Title, Msg:string);
var
  MsgForm: TMsgForm;
begin
  MsgForm :=  TMsgForm.Create(NIL);
  MsgForm.mMessage.Text := msg;
  MsgForm.ShowForm(title);
end;

end.
